ARG CI_REGISTRY
FROM debian:buster

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install openjdk-10-jre
